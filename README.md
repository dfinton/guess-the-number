# README #

There are the source code files I either modified or created to make this project:

```
 css/app.css
 local/app/Http/Controllers/HomeController.php
 local/app/Http/routes.php
 local/app/Managers/GameManager.php
 local/app/Mappers/GameMapper.php
 local/app/Game.php
 local/app/Validators/GuessValidator.php
 local/database/migrations/2016_04_24_190836_create_games_table.php
 local/database/seeds/DatabaseSeeder.php
 local/database/seeds/UserTableSeeder.php
 local/resources/views/home.blade.php
 local/resources/views/index.blade.php
 local/resources/views/layouts/app.blade.php
 local/tests/GuessTest.php
 local/tests/ManagerTest.php
 local/tests/MapperTest.php
```