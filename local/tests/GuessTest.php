<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GuessTest extends TestCase
{
    // To roll back our test users created for the test
    use DatabaseTransactions; 

    /**
     * Test the main index page and see that it loads
     *
     * @return void
     */
    public function testIndexPage()
    {
        $this->visit('/')
            ->see('Guess The Number');
    }

    /**
     * /home should redirect to the login page unless authenticated
     *
     * @return void
     */
    public function testHomePageNoAuth()
    {
        $this->visit('/home')
            ->seePageIs('/login');
    }

    /**
     * /home should display properly once authenticated
     *
     * @return void
     */
    public function testHomePageAuth()
    {
        $user = factory(App\User::class)->create();

        $this->actingAs($user)
            ->visit('/home')
            ->seePageIs('/home');
    }

    /**
     * Test the guess form with blank string
     *
     * @return void
     */
    public function testGuessFormBlank()
    {
        $user = factory(App\User::class)->create();

        $this->actingAs($user)
            ->visit('/home')
            ->press('Guess it')
            ->see('The guess field is required.');
    }

    /**
     * Test the guess form with alpha string
     *
     * @return void
     */
    public function testGuessFormAlpha()
    {
        $user = factory(App\User::class)->create();

        $this->actingAs($user)
            ->visit('/home')
            ->type('tree', 'guess')
            ->press('Guess it')
            ->see('The guess must be an integer.');
    }

    /**
     * Test the guess form with an out-of-range value
     *
     * @return void
     */
    public function testGuessFormTooSmall()
    {
        $user = factory(App\User::class)->create();

        $this->actingAs($user)
            ->visit('/home')
            ->type('0', 'guess')
            ->press('Guess it')
            ->see('The guess must be at least 1.');
    }

    /**
     * Test the guess form with an out-of-range value
     *
     * @return void
     */
    public function testGuessFormTooLarge()
    {
        $user = factory(App\User::class)->create();

        $this->actingAs($user)
            ->visit('/home')
            ->type('15', 'guess')
            ->press('Guess it')
            ->see('The guess may not be greater than 10.');
    }

    /**
     * Test the guess form with valid input
     *
     * @return void
     */
    public function testGuessForm()
    {
        $user = factory(App\User::class)->create();

        $this->actingAs($user)
            ->visit('/home')
            ->type('5', 'guess')
            ->press('Guess it')
            ->see(' guess is: ');
    }
}
