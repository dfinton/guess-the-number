<?php

use App\Mappers\GameMapper;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MapperTest extends TestCase
{
    // To roll back our test users created for the test
    use DatabaseTransactions; 

    /**
     * The mapper, once initilaized, should have certain values set/not set
     *
     * @return void
     */
    public function testDefaultMapper() {
        $mapper = new GameMapper();
        $mapperData = $mapper->toArray();

        $expected = array(
            'isActive' => false,
            'lastGuess' => null,
            'guessOffBy' => null,
            'numGuesses' => null,
            'correct' => null,
        );

        $this->assertSame($expected, $mapperData, 'Default mapper data does not match expected');
    }

    /**
     * We set up arbitrary games in progress and make sure the mapper maps correctly
     *
     * @return void
     */
    public function testMapperWithData() {
        $gameDataArray = array(
            'In Progress' => array(
                'correct' => 5,
                'guesses' => 1,
                'lastGuess' => 4,
                'isWon' => false,
                'isViewed' => false,
            ),
            'Game Lost' => array(
                'correct' => 5,
                'guesses' => 3,
                'lastGuess' => 4,
                'isWon' => false,
                'isViewed' => false,
            ),
            'Game Won' => array(
                'correct' => 5,
                'guesses' => 1,
                'lastGuess' => 5,
                'isWon' => true,
                'isViewed' => false,
            ),
        );

        $user = factory(App\User::class)->create();

        foreach ($gameDataArray as $dataLabel => $gameData) {
            $correct = $gameData['correct'];
            $guesses = $gameData['guesses'];
            $lastGuess = $gameData['lastGuess'];
            $isWon = $gameData['isWon'];
            $isViewed = $gameData['isViewed'];

            $mapper = new GameMapper();

            $game = factory(App\Game::class)->make(array(
                'correct' => $correct,
                'guesses' => $guesses,
                'last_guess' => $lastGuess,
                'is_won' => $isWon,
                'is_viewed' => $isViewed,
                'user_id' => $user->id,
            ));

            $mapper->map($game);
            $mapperData = $mapper->toArray();

            $expected = array(
                'isActive' => true,
                'lastGuess' => $lastGuess,
                'guessOffBy' => abs($correct - $lastGuess),
                'numGuesses' => $guesses,
                'correct' => $correct,
            );

            $this->assertSame($expected, $mapperData, "Mapper data does not match expected for game '{$dataLabel}'");
        }
    }
}
