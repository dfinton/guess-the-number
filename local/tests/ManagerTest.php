<?php

use App\Managers\GameManager;
use App\Mappers\GameMapper;
use App\Validators\GuessValidator;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ManagerTest extends TestCase
{
    // To roll back our test users created for the test
    use DatabaseTransactions; 

    /**
     * Test of the getValidator method via mock
     *
     * @return void
     */
    public function testGetValidator() {
        $stubGameMapper = $this->getMockBuilder(GameMapper::class)
            ->getMock();

        $stubGuessValidator = $this->getMockBuilder(GuessValidator::class)
            ->setMethods(array('getValidator'))
            ->getMock();

        $stubGuessValidator->expects($this->once())
            ->method('getValidator')
            ->will($this->returnValue(true));

        $gameManager = new GameManager($stubGameMapper, $stubGuessValidator);
        $validator = $gameManager->getValidator();

        $this->assertTrue($validator, 'Method GameManager::getValidator did not return the stubbed value');
    }
}
