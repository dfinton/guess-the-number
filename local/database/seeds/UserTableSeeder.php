<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder {
    /**
     * Create a few users
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        User::create(array(
            'name' => 'Admin',
            'email' => 'admin@test.com',
            'password' => bcrypt('test1234'),
        ));

        User::create(array(
            'name' => 'User 1',
            'email' => 'user1@test.com',
            'password' => bcrypt('test1234'),
        ));

        User::create(array(
            'name' => 'User 2',
            'email' => 'user2@test.com',
            'password' => bcrypt('test1234'),
        ));
    }
}
