<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    /**
     * When creating a new record, this method will set the appropriate values
     * so that the object can be saved
     *
     * @param int $userId
     * @return Game
     */
    public function init($userId) {
        $this->correct = rand(1, 10);
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Determine if a guess is correct and update the object appropriately
     *
     * @param int $guess
     * @return Game
     */
    public function processGuess($guess) {
        $isCorrectGuess = ($this->correct == $guess);

        $this->is_won = $isCorrectGuess;
        $this->guesses++;
        $this->last_guess = $guess;

        return $this;
    }

    /**
     * If a game is completed, once it is viewed we don't want to see it again.
     * Returns true if the flag had to be flipped
     *
     * @return boolean
     */
    public function setViewedIfCompleted() {
        if (!$this->is_viewed && (3 == $this->guesses || $this->is_won)) {
            $this->is_viewed = true;

            return true;
        }

        return false;
    }

    /**
     * Scoping function for our query builder: returns all active games
     *
     * @param \Illuminate\Database\Query
     * @param int $userId
     * @return \Illuminate\Database\Query
     */
    public function scopeActive($query, $userId) {
        return $this->where('user_id', $userId)
            ->where('is_won', false)
            ->where('guesses', '<', 3)
            ->orderBy('created_at', 'desc');
    }

    /**
     * Scoping function for our query builder: returns all completed, unviewed 
     * games
     *
     * @param \Illuminate\Database\Query
     * @param int $userId
     * @return \Illuminate\Database\Query
     */
    public function scopeUnviewed($query, $userId) {
        return $this->where('user_id', $userId)
            ->where('is_viewed', false)
            ->where('guesses', '<=', 3)
            ->orderBy('created_at', 'desc');
    }
}
