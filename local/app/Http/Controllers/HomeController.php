<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Managers\GameManager;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Our Guess Manager is responsible for managing high-level applcation
     * logic
     *
     * @var GameManager $gameManager
     */
    var $gameManager;

    /**
     * Create a new controller instance.
     *
     * @param GuessValidator $guessValidator
     * @return void
     */
    public function __construct(GameManager $gameManager)
    {
        // Validate the login session; if this fails we redirect to the login 
        // page
        $this->middleware('auth');

        // Setting our dependency injections
        $this->gameManager = $gameManager;
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $activeGameData = $this->gameManager->getActiveGameData($user);

        return view('home', $activeGameData);
    }

    /**
     * Handle POST action, then redirects to the dashboard
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function post(Request $request)
    {
        // Check to make sure the input field makes sense; if not do not 
        // register a guess attempt and flash an error message
        $validator = $this->gameManager->getValidator($request->all());

        if ($validator->fails()) {
            return redirect('home')
                ->withErrors($validator);
        }

        // Since we have a valid input now, check to see if the guess is
        // correct. We store the result in the database and display the result
        // to the player
        $user = $request->user();
        $guess = (int)($request->input('guess'));
        $result = $this->gameManager->processGuess($user, $guess);

        // Application logic has now been processed, we redirect back to home
        return redirect('home');
    }
}
