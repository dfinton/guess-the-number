<?php

namespace App\Validators;

use Validator;

class GuessValidator {
    var $validator;

    /**
     * Returns a Laravel Validator object using pre-defined rules.
     * If no inputs are provided, it will return the previously
     * generated validator object, or null if there is none
     *
     * @param array $inputs
     * @return Validator|null
     */
    public function getValidator($inputs = null) {
        if (is_array($inputs)) {
            $this->validator = Validator::make($inputs, array(
                'guess' => 'required|integer|min:1|max:10',
            ));
        }

        return $this->validator;
    }
}
