<?php

namespace App\Managers;

use App\Mappers\GameMapper;
use App\Validators\GuessValidator;
use App\Game;

class GameManager {
    /**
     * Makes a Game object and converts it to data our Controller can
     * use to render the view
     *
     * @var GameMapper $gameMapper
     */
    var $gameMapper;

    /**
     * Our Guess Validator wrapper class that handles the specific
     * validation rules for the inputed value(s)
     *
     * @var GuessValidator $guessValidator
     */
    var $guessValidator;

    /**
     * Create a new controller instance.
     *
     * @param GameMapper $gameMapper
     * @param GuessValidator $guessValidator
     * @return void
     */
    public function __construct(GameMapper $gameMapper, GuessValidator $guessValidator)
    {
        // Setting our dependency injections
        $this->guessValidator = $guessValidator;
        $this->gameMapper = $gameMapper;
    }

    /**
     * Returns a Laravel Validator object using pre-defined rules.
     *
     * @param array $inputs
     * @return \Validator|null
     */
    public function getValidator($inputs = null) {
        return $this->guessValidator->getValidator($inputs);
    }

    /**
     * Takes a guess and see if it correct or not
     *
     * @param \App\User $user
     * @param int $guess
     * @return void
     */
    public function processGuess($user, $guess) {
        $activeGameQuery = Game::active($user->id);

        if (0 == $activeGameQuery->count()) {
            $activeGameRecord = new Game;
            $activeGameRecord->init($user->id);
        } else {
            $activeGameRecord = $activeGameQuery->first();
        }

        $activeGameRecord->processGuess($guess);
        $activeGameRecord->save();
    }

    /**
     * Get current game data. If the game is completed, then we only retrieve
     * the data if it hasn't already been viewed. Once it is pulled once, the
     * is_viewed flag for the model is set.
     *
     * @param \App\User $user
     * @return array
     */
    public function getActiveGameData($user) {
        $activeGameQuery = Game::unviewed($user->id);

        if (0 == $activeGameQuery->count()) {
            return $this->gameMapper->toArray();
        }

        $activeGameRecord = $activeGameQuery->first();
        $this->gameMapper->map($activeGameRecord);

        if ($activeGameRecord->setViewedIfCompleted()) {
            $activeGameRecord->save();
        }

        return $this->gameMapper->toArray();
    }
}
