<?php

namespace App\Mappers;

use App\Game;

class GameMapper extends Mapper {
    /**
     * True if there are turns remaining in the game and it hasn't been won yet
     *
     * @var boolean $isActive
     */
    var $isActive;

    /**
     * The last entered guess in this game
     *
     * @var int $lastGuess
     */
    var $lastGuess;

    /**
     * How far the guess is from the correct value
     *
     * @var int $guessOffBy
     */
    var $guessOffBy;

    /**
     * Total number of guesses entered so far
     *
     * @var int $numGuesses
     */
    var $numGuesses;

    /**
     * The correct value that must be guess to win the game
     *
     * @var int $correct
     */
    var $correct;

    /**
     * Constructor method to set the default values for some private variables
     *
     * @return void
     */
    public function __construct() {
        $this->isActive = false;
    }

    /**
     * Maps a given object that extends from an Eloquent model to our object
     *
     * @param Game $gameRecord
     * @return GameMapper
     */
    public function map(Game $record) {
        $guessOffBy = abs($record->last_guess - $record->correct);

        $this->isActive = true;
        $this->lastGuess = $record->last_guess;
        $this->guessOffBy = $guessOffBy;
        $this->numGuesses = $record->guesses;
        $this->correct = $record->correct;

        return $this;
    }
}
