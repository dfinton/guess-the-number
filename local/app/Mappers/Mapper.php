<?php

namespace App\Mappers;

class Mapper {
    /**
     * Base method for converting the private variables in this object 
     * to an array
     *
     * @return array
     */
    public function toArray() {
        return get_object_vars($this);
    }
}
