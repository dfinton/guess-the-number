@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    <div>
                        This is the "Guess The Number" game application. You have three attempts to guess a number between 1 and 10.
                        You'll be told if you are cold, warm, or hot based on how close to the correct number your guess is.
                        Since the game is dependent on sessions, you'll need to log in with an email and password so that the
                        game can track your progress and allow you to log out and log back in to resume an existing game.
                    </div>

                    <div class="top-buffer">
                        <a href="{{ url('home') }}">Click here to begin!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
