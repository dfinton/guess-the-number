@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Guess The Number</div>

                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div>I am thinking of a number from 1 to 10</div>
                    <div>You must guess what it is in three tries</div>
                    <div>
                        <form role="form" method="POST" action="{{ url('home') }}">
                            {!! csrf_field() !!}
                            <label>Enter a Guess</label>
                            <input type="text" name="guess" id="guess"></input>
                            <input type="submit" value="Guess it"></input>
                        </form>
                    </div>
                    @if ($isActive)
                        <div>
                            @if ($numGuesses == 1)
                                Your first guess is: {{ $lastGuess }}
                            @elseif ($numGuesses == 2)
                                Your second guess is: {{ $lastGuess }}
                            @elseif ($numGuesses == 3)
                                Your last guess is: {{ $lastGuess }}
                            @endif

                            @if ($guessOffBy == 1)
                                (hot)
                            @elseif ($guessOffBy == 2)
                                (warm)
                            @elseif ($guessOffBy >= 3)
                                (cold)
                            @endif
                        </div>

                        @if ($guessOffBy == 0)
                            Right! You have won the game!
                        @elseif ($numGuesses >= 3)
                            You're out of guesses! The correct value was {{ $correct }}. To play again with a new value, enter another guess.
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
